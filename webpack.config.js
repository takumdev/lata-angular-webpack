var path = require('path');
var autoprefixer = require('autoprefixer');
var HtmlwebpackPlugin = require('html-webpack-plugin');
var merge = require('webpack-merge');
var webpack = require('webpack');
var Clean = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var pkg = require('./package.json');

const TARGET = process.env.npm_lifecycle_event;
const PATHS = {
  app: path.join(__dirname, 'app'),
  build: path.join(__dirname, 'build'),
  test: path.join(__dirname, 'test')
};

process.env.BABEL_ENV = TARGET;

const common = {
  entry: path.resolve(PATHS.app, 'app'),
  resolve: {
    extensions: ['', '.js'],
    modulesDirectories: ["node_modules", "bower_components"]
  },
  output: {
    path: PATHS.build,
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ['babel'],
        include: PATHS.app
      },
      {
        test: /\.html$/,
        loader: 'ng-cache?prefix=[dir]',
        include: path.resolve(PATHS.app, 'modules')
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'file?name=images/[name].[ext]',
        include: path.resolve(PATHS.app, 'assets')
      },
      {
        test: /\.(svg|woff|woff2|ttf|eot)$/,
        loader: 'file?name=fonts/[name].[ext]',
        include: [
          path.resolve(PATHS.app, 'assets'),
          path.resolve(__dirname, "node_modules/bootstrap")
        ]
      }
    ]
  },
  plugins: [
    new HtmlwebpackPlugin({
      title: 'WebPage Title',
      template: 'templates/index.tpl'
    })
  ]
};

if(TARGET === 'start' || !TARGET) {
  module.exports = merge(common, {
    devtool: 'eval-source-map',
    devServer: {
      historyApiFallback: true,
      hot: true,
      inline: true,
      progress: true,

      // display only errors to reduce the amount of output
      stats: 'errors-only',

      // parse host and port from env so this is easy
      // to customize
      host: process.env.HOST,
      port: process.env.PORT
    },
    module: {
      loaders: [
        // Define development specific CSS setup
        {
          test: /\.less$/,
          loaders: ['style', 'css', 'postcss', 'less'],
          include: PATHS.app
        }
      ]
    },
    postcss: [
      autoprefixer({ browsers: ['last 2 versions'] })
    ],
    plugins: [
      new webpack.HotModuleReplacementPlugin()
    ]
  });
}

if(TARGET === 'build' || TARGET === 'stats' || TARGET === 'deploy') {
  module.exports = merge(common, {
    // Define entry points needed for splitting
    entry: {
      app: path.resolve(PATHS.app, 'app'),
      vendor: Object.keys(pkg.dependencies)
    },
    output: {
      path: PATHS.build,
      // Output using entry name
      filename: '[name].[chunkhash].js',
      chunkFilename: '[chunkhash].js'
    },
    module: {
      loaders: [
        // Extract CSS during build
        {
          test: /\.less$/,
          loader: ExtractTextPlugin.extract('style', 'css!postcss!less'),
          include: PATHS.app
        }
      ]
    },
    plugins: [
      new Clean([PATHS.build]),
      // Output extracted CSS to a file
      new ExtractTextPlugin('styles.[chunkhash].css'),
      // Extract vendor and manifest files
      new webpack.optimize.CommonsChunkPlugin({
        names: ['vendor', 'manifest']
      }),
      // Setting DefinePlugin affects React library size!
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      })
    ]
  });
}

if(TARGET === 'test' || TARGET === 'tdd') {
  module.exports = merge(common, {
    entry: {}, // karma will set this
    output: {}, // karma will set this
    devtool: 'inline-source-map',
    resolve: {
      alias: {
        'app': PATHS.app
      }
    },
    module: {
      preLoaders: [
        {
          test: /\.jsx?$/,
          loaders: ['isparta-instrumenter'],
          include: PATHS.app
        }
      ],
      loaders: [
        {
          test: /\.jsx?$/,
          loaders: ['babel'],
          include: PATHS.test
        }
      ]
    }
  });
}
