// Uncomment when not using less
// import 'bootstrap/dist/fonts/glyphicons-halflings-regular.eot';
// import 'bootstrap/dist/fonts/glyphicons-halflings-regular.svg';
// import 'bootstrap/dist/fonts/glyphicons-halflings-regular.ttf';
// import 'bootstrap/dist/fonts/glyphicons-halflings-regular.woff';
// import 'bootstrap/dist/fonts/glyphicons-halflings-regular.woff2';
// import 'bootstrap/dist/css/bootstrap.css';
// When using less
import './styles/app.less';

import angular from 'angular';
import uirouter from 'angular-ui-router';
import uibootstrap from 'angular-ui-bootstrap';

import routing from './app.config';
import layout from './modules/layout';

angular.module('app',
[
  uirouter,
  uibootstrap,
  layout.name
]).config(routing);
