export default class HomeController {
  constructor (randomNames) {
    this.random = randomNames;
    this.glyphiconNames = this.random.getName();
  }

  changeName() {
    this.glyphiconNames = this.random.getName();
  }
}

HomeController.$inject = ['randomNames'];
