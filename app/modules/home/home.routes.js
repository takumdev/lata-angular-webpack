import './home.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('app.index', {
      url: '/',
      templateUrl: 'home/home.html',
      controller: 'HomeController',
      controllerAs: 'home'
    });
}

routes.$inject = ['$stateProvider'];
