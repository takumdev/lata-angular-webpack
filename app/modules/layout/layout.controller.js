export default class LayoutController {
  constructor (randomNames) {
    this.isCollapsed = true;
    this.random = randomNames;
    this.name = 'World';
  }

  changeName() {
    this.name = this.random.getName();
  }
}

LayoutController.$inject = ['randomNames'];
