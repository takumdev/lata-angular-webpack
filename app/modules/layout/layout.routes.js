import './layout.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('app', {
      abstract: true,
      templateUrl: 'layout/layout.html',
      controller: 'LayoutController',
      controllerAs: 'nav'
    });
}

routes.$inject = ['$stateProvider'];
