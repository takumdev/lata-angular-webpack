import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './layout.routes';
import LayoutController from './layout.controller';

import home from '../home';
import page from '../page';

export default angular.module('app.layout', [
  uirouter,
  home.name,
  page.name,
])
.config(routing)
.controller('LayoutController', LayoutController);
