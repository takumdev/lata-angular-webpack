import './page.html';

export default function routes($stateProvider) {
  $stateProvider
    .state('app.page', {
      url: '/page',
      templateUrl: 'page/page.html'
    });
}

routes.$inject = ['$stateProvider'];
