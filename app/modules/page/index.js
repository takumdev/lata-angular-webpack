import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from './page.routes';

export default angular.module('app.page', [
  uirouter
])
.config(routing);
// .controller('LayoutController', LayoutController); // should add controller if required
