import angular from 'angular';
//I did this just for fun! Do not use-it as the file can change over the time
var fileContent = require("raw!../../node_modules/bootstrap/less/glyphicons.less");
const _shuffle = require('lodash-node/modern/collection/shuffle');
const _take = require('lodash-node/modern/array/take');

class RandomNames {
  constructor () {
    // filter the two icons that are not working, see the file:
    // glyphicons.less for more information, line: 242
    this.names = fileContent.match(/glyphicon(-\w*)+/g).filter((icon)=>{
      return icon !== 'glyphicon-door' || icon !== 'glyphicon-key';
    });
  }
  getName () {
    let newArr = _shuffle(this.names);
    return _take(newArr, 8);
  }
}

export default angular.module('services.random-names', [])
.service('randomNames', RandomNames);
