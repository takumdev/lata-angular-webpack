# lata-angular-webpack

This is a boilerplate for development with: Angular, Webpack, Babel and Less.

Also I'm using [ng-cache-loader](https://github.com/teux/ng-cache-loader) for $templateCache

This work is based on [surviveJS](http://survivejs.com/) and [angular-tips post] (http://angular-tips.com/blog/2015/06/using-angular-1-dot-x-with-es6-and-webpack/)

TODO: testing, sorry didn't have much time to add testing process.
